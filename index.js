var express = require('express');
var app = express();

const fs = require('fs'),
    path = require('path'),
    twit = require('twit'),
    config = require(path.join(__dirname,'./config.js'));
    fetch = require('node-fetch'),
    url = 'https://oyashirosama.herokuapp.com/'

const links = require('./links.json');

const twitter = new twit(config);

// const cron = require('node-cron');

// const cronJob = cron.schedule('0 */25 * * * *', () => {
//     fetch(url)
//         .then(res => console.log(`Response OK, hauuauu??: ${res.ok}, Here's the status: ${res.status}`))
//         .catch(err => console.error(err));
// });

app.set('port', (process.env.PORT || 5000));


app.get('/', function (request, response) {
    var result = `Application is up and running.`;

    response.send(result);
}).listen(app.get('port'), function () {
    console.log("Application is attempting to run.. server is listening on PORT:", app.get('port'));
})

function randomFromArray ( links ){
    return links[Math.floor(Math.random() * links.length)];
}

function tweetRandomImage(){

        const randomImage = randomFromArray(links)

        console.log('Fetched link, uploading...');

        twitter.post('statuses/update', {status: randomImage}, function (err,data,response) {
            console.log('Tweet uploaded, hauuauu!');
            if (err){
                console.log('There was an error, I\'m sorry, hauuauu...', err);
                tweetRandomImage();
            }});
    }

setInterval(function(){
    tweetRandomImage();
}, 10800000);