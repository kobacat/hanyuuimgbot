const fs = require('fs'),
    path = require('path'),
    twit = require('twit'),
    config = require(path.join(__dirname,'./config.js'));

const twitter = new twit(config);

function randomFromArray ( images ){
    return images[Math.floor(Math.random() * images.length)];
}

function tweetRandomImage(){
    fs.readdir(__dirname + '/images', function(err,files) {
        if (err){
            console.log('There was an error, I\'m sorry, hauuauu...', err);
            tweetRandomImage();
        }
        else {
            let images = [];
            files.forEach(function(f) {
                images.push(f);
            });

        console.log('Image fetched from folder...');

        const randomImage = randomFromArray(images)

        const imagePath = path.join(__dirname, '/images/' + randomImage),
              b64content = fs.readFileSync(imagePath, {encoding: 'base64'});

        console.log(randomImage);

        console.log('Image uploading to Twitter...');

        twitter.post('media/upload', {media_data: b64content}, function (err,data,response) {
            if (err){
                console.log('There was an error, I\'m sorry, hauuauu...', err);
                tweetRandomImage();
            }
            else{
                console.log('Tweet being sent out nanodesu...');

                twitter.post ('statuses/update', {
                    media_ids: new Array(data.media_id_string)
                },
                    function(err,data,response) {
                        if (err){
                            console.log('There was an error, I\'m sorry, hauuauu...', err);
                            tweetRandomImage();
                        }
                        else{
                            console.log(`Image posted to Twitter at ${Date()}, hauuauu!!`)
                        }
                    }
                );
            }
        });
    }
    });
}
setInterval(function(){
    tweetRandomImage();
}, 10800000);